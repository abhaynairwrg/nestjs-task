import { Injectable, Logger } from '@nestjs/common';
import {
  Cron,
  CronExpression,
  Interval,
  SchedulerRegistry,
  Timeout,
} from '@nestjs/schedule';
import { CronJob } from 'cron';

@Injectable()
export class TasksService {
  constructor(private schedulerRegistry: SchedulerRegistry) {}

  private readonly logger = new Logger(TasksService.name);

  @Cron(CronExpression.EVERY_10_SECONDS, {
    name: 'tenSecondCron',
  })
  handleCron() {
    this.logger.debug('Called every 10 seconds');
  }

  @Interval('OneSecondInterval', 1000)
  handleInterval() {
    this.logger.debug('Called every 1 seconds');
  }

  @Timeout('ThirtySecondTimeOut', 30000)
  handleTimeout() {
    const job = this.schedulerRegistry.getCronJob('tenSecondCron');

    // end old cron
    job.stop();
    console.log(job.lastDate());

    this.logger.debug('Cron job of 10 seconds will end after this');

    // start new cron
    this.addCronJob('ThirtySecondCron');

    // delete everything else
    this.deleteIntervalAndTimeout();
  }

  addCronJob(name: string) {
    const job = new CronJob(CronExpression.EVERY_30_SECONDS, () => {
      this.logger.warn(`30 second cron running`);
    });

    this.schedulerRegistry.addCronJob(name, job);
    job.start();

    this.logger.warn(`job ${name} added for each minute at 30 seconds!`);
  }

  deleteIntervalAndTimeout() {
    this.schedulerRegistry.deleteInterval('OneSecondInterval');
    this.logger.warn(`Interval OneSecondInterval deleted!`);

    this.schedulerRegistry.deleteTimeout('ThirtySecondTimeOut');
    this.logger.warn(`Timeout ThirtySecondTimeOut deleted!`);
  }
}
